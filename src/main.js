import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import ethersPlugin from './plugins/ethers'
import JsonViewer from 'vue-json-viewer'

Vue.use(JsonViewer)
Vue.config.productionTip = false
Vue.use(ethersPlugin);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
