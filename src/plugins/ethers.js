import { ethers } from "ethers";

const ethersPlugin = {
    install: function (Vue, options) {

        const getMetaMask = () => {
            return window.ethereum ? window.ethereum : {}
        }
        let checkConnectionTimerId;
        Vue.prototype.$ethers = ethers;
        Vue.prototype.$metaMask = getMetaMask();

        Vue.prototype.$getAccounts = async (provider) => {
            return await provider.listAccounts().then((account) => {
                if (account) {
                    return account
                } else {
                    return ""
                }

            });
        };

        Vue.filter('bigNumber', function (balance) {
            return ethers.utils.formatUnits(balance, 18) 
        })
        
        Vue.prototype.$parseToUint = function ( sum ) {
            return this.$ethers.utils.parseUnits(sum)
        }

        Vue.prototype.$disconnect = async function () {
            return new Promise((res)=>{
                checkConnectionTimerId = setInterval(async ()=>{
                    this.$metaMask.on("accountsChanged", accounts => {
                        if (accounts.length <= 0) {
                            clearInterval(checkConnectionTimerId)
                            res()
                        }
                    });
                },1000)
            })
        }

        Vue.prototype.$getJsonRpcProvider = function () {
            return new ethers.providers.Web3Provider(this.$metaMask);
        }

        Vue.prototype.$createContract = ({hash, contractScheme, provider, account}) => {
                const contract = new ethers.Contract(hash, contractScheme, provider.getSigner(account))
                return contract;           
        }
         
        Vue.prototype.$connectWallet = function(onConnect) {
            this.$metaMask
                .request({ method: 'eth_requestAccounts' })
                .then(onConnect)
                .catch((error) => {
                    if (error.code === 4001) {
                        onConnect({error: true});
                        // EIP-1193 userRejectedRequest error
                        console.log('Please connect to MetaMask.');
                    } else {
                        console.error(error);
                    }
                });
        }

    }
}

export default ethersPlugin;