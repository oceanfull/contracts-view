import Vue from 'vue'
import VueRouter from 'vue-router'
import StackUnstack from '../views/StackUnstack'
import Contract from '../views/Contract'

Vue.use(VueRouter)

const routes = [
  {
    path: '/stack',
    name: 'stack',
    component: StackUnstack
  },
  {
    path: '/',
    redirect: '/contract'
  },
  {
    path: '/contract',
    name: 'contract',
    component: Contract
  }
]

const router = new VueRouter({
  routes,
  mode: "history"
})

export default router
